import { Component, OnInit, Input } from '@angular/core';


interface ITab {
  title: string;
  removable: boolean;
  disabled: boolean;
  active: boolean;
}

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent  {
  @Input() _Menu: any;

  constructor() { }

  tabs: ITab[]=[

  ];


  addNewTab(): void {
      var menuName;
    for(let i=0; i<this._Menu.length; i++)
    {
      if(this._Menu[i][1]==1)
      {
        menuName=this._Menu[i][0];
      }
    }

    this.tabs.push({
      title: menuName,
      disabled: false,
      removable: true,
      active:true,
    });
  }


  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }

}
