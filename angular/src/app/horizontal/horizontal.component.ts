import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-horizontal',
  templateUrl: './horizontal.component.html',
  styleUrls: ['./horizontal.component.scss']
})
export class HorizontalComponent implements OnInit {
  @Input() _Menu: any;
  @Output() onOpenTab= new  EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

OpentMenu( _MenuName: any){
  for(let i=0; i<this._Menu.length;i++)
  {
    this._Menu[i][1]=0;
  }

  for(let i=0; i<this._Menu.length;i++)
  {
    if(this._Menu[i][0]==_MenuName)
    {
        this._Menu[i][1]=1;
    }
  }

  this.onOpenTab.emit(1);

}
}


